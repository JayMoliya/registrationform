package com.example.registrationformapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class DetailsActivity : AppCompatActivity() {

    lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        textView = findViewById(R.id.tvUserDetails)

        val intent = intent
        with(intent) {
            val userName = getStringExtra("name")
            val email = getStringExtra("email")
            val phone = getStringExtra("phone")
            val password = getStringExtra("password")
            val confirmPassword = getStringExtra("confirmPassword")

            ("UserName : " + userName +
                    "\nPhoneNo : " + phone +
                    "\nEmail :" + email +
                    "\nPassword : " + password +
                    "\nConfirmPassword : " + confirmPassword).also { textView.text = it }

        }
    }
}