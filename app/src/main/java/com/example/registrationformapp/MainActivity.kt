package com.example.registrationformapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val userName = findViewById<EditText>(R.id.evName)
        val userPhone = findViewById<EditText>(R.id.evPhone)
        val userEmail = findViewById<EditText>(R.id.evEmail)
        val userPassword = findViewById<EditText>(R.id.evPassword)
        val userConfirmPassword = findViewById<EditText>(R.id.evConfirmPassword)
        val btnRegister = findViewById<Button>(R.id.btnRegister)

        btnRegister.setOnClickListener {
            val name = userName.text.toString()
            val phone = userPhone.text.toString()
            val email = userEmail.text.toString()
            val password = userPassword.text.toString()
            val confirmPassword = userConfirmPassword.text.toString()

            val intent = Intent(this,DetailsActivity::class.java)
            intent.apply {
                putExtra("name",name)
                putExtra("email",email)
                putExtra("phone",phone)
                putExtra("password",password)
                putExtra("confirmPassword",confirmPassword)
                startActivity(intent)
            }
        }


    }
}